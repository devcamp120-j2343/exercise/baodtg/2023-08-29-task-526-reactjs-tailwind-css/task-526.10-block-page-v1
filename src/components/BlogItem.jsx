const BlogItem = ({ data }) => {
    return (
        <div className="flex flex-col mt-[50px]">
            <img src={data.image} alt="blogImg" className="rounded-lg" />
            <p className="bg-blue-800 max-w-[150px] text-center rounded-lg my-7 text-white py-2" >{data.time}</p>
            <h1 className="text-[24px] font-[600] mb-[15px]">{data.title}</h1>
            <h5 className="text-[16px] font-[400] mb-[15px] text-[#637381]">{data.description}</h5>
        </div>
    )
}
export default BlogItem


import BlogItem from "../components/BlogItem"
import items from "../data"

const Blog = () => {
    return (
        <>
            <div className="text-[#3056D3] text-center text-lg mt-[120px] font-semibold" >Our Blogs</div>
            <div className="text-center text-[#2E2E2E] font-[700] text-[40px] mt-[8px] " >Our Recent News</div>
            <div className="text-[#637381] text-center font-[400] text-[15px] max-w-[480px] mx-auto " >There are many variations of passages of Lorem Ipsum available
                but the majority have suffered alteration in some form.</div>
            <div className="grid grid-cols-3 max-w-[80%] mx-auto gap-[30px]" >
                {
                    items.map((item) => {
                        return <BlogItem data={item} />
                    })
                }

            </div>

        </>
    )



}
export default Blog
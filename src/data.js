import img1 from './assets/images/Blog Image.jpg'
import img2 from './assets/images/Blog Image (1).jpg'
import img3 from './assets/images/Blog Image (2).jpg'

const items = [
    {
        image: img1,
        time: "Dec 22, 2023",
        title: "Meet AutoManage, the best AI management tools",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
    },
    {
        image: img2,
        time: "Mar 15, 2023",
        title: "How to earn more money as a wellness coach",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
    },
    {
        image: img3,
        time: "Jan 05, 2023",
        title: "The no-fuss guide to upselling and cross selling",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
    }
]
export default items